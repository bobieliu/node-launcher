apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "thor-daemon.fullname" . }}
  labels:
    {{- include "thor-daemon.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: {{ .Values.strategyType }}
  selector:
    matchLabels:
      {{- include "thor-daemon.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "thor-daemon.selectorLabels" . | nindent 8 }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "thor-daemon.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}

      {{- if .Values.hostNetwork }}
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      {{- end }}

      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}

      initContainers:
      - name: init-external-ip
        image: alpine/k8s:1.18.2
        {{- if .Values.global.gateway.enabled }}
        command: ['/scripts/external-ip.sh', '{{ .Values.hostNetwork }}', '{{ .Values.global.gateway.name }}', '{{ include "thor-daemon.fullname" . }}-external-ip']
        {{- else }}
        command: ['/scripts/external-ip.sh', '{{ .Values.hostNetwork }}', '{{ include "thor-daemon.fullname" . }}', '{{ include "thor-daemon.fullname" . }}-external-ip']
        {{- end }}
        volumeMounts:
        - name: scripts
          mountPath: /scripts
      {{- if default .Values.peer .Values.global.peer }}
      - name: init-peer
        image: busybox:1.28
        command: ['sh', '-c', 'until nc -zv {{ default .Values.peer .Values.global.peer }}:{{ include "thor-daemon.rpc" . }}; do echo waiting for peer thor-daemon; sleep 2; done']
      {{- end }}
      {{- if .Values.peerApi }}
      - name: init-peer-api
        image: busybox:1.28
        command: ['sh', '-c', "until nc -zv {{ .Values.peerApi }}:1317; do echo waiting for peer thor-api; sleep 2; done"]
      {{- end }}
      {{- if default .Values.binanceDaemon .Values.global.binanceDaemon }}
      - name: init-binance
        image: appropriate/curl
        command: ['sh', '-c', "until curl -s {{ default .Values.binanceDaemon .Values.global.binanceDaemon }} > /dev/null; do echo waiting for binance-daemon; sleep 2; done"]
      {{- end }}
      - name: init-thor-daemon
        image: {{ include "thor-daemon.image" . }}
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        {{- if or (default .Values.peer .Values.global.peer) .Values.seeds }}
        command: ["/scripts/validator.sh"]
        {{- else }}
        command: ["/scripts/genesis.sh"]
        {{- end }}
        volumeMounts:
          - name: data
            mountPath: /root/
        env:
          - name: EXTERNAL_IP
            valueFrom:
              configMapKeyRef:
                name: {{ include "thor-daemon.fullname" . }}-external-ip
                key: externalIP
          - name: PEER
            value: {{ default .Values.peer .Values.global.peer }}
          - name: VALIDATOR
            value: "{{ .Values.validator }}"
          - name: SEEDS
            value: {{ .Values.seeds }}
          - name: PEER_API
            value: {{ .Values.peerApi }}
          - name: BINANCE
            value: {{ default .Values.binanceDaemon .Values.global.binanceDaemon }}
          - name: SEED
            valueFrom:
              fieldRef:
                {{- if .Values.hostNetwork }}
                fieldPath: spec.nodeName
                {{- else}}
                fieldPath: metadata.name
                {{- end }}
          - name: NET
            value: {{ include "thor-daemon.net" . }}
          - name: SIGNER_NAME
            value: {{ .Values.signer.name }}
          - name: SIGNER_PASSWD
            {{- if default .Values.signer.passwordSecret .Values.global.passwordSecret }}
            valueFrom:
              secretKeyRef:
                name: {{ default .Values.signer.passwordSecret .Values.global.passwordSecret }}
                key: password
            {{- else}}
            value: {{ .Values.signer.password }}
            {{- end }}
          {{- if default .Values.signer.mnemonicSecret .Values.global.mnemonicSecret }}
          - name: SIGNER_SEED_PHRASE
            valueFrom:
              secretKeyRef:
                name: {{ default .Values.signer.mnemonicSecret .Values.global.mnemonicSecret }}
                key: mnemonic
          {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ include "thor-daemon.image" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: ["/kube-scripts/entrypoint.sh"]
          volumeMounts:
            - name: data
              mountPath: /root/
            - name: scripts
              mountPath: /kube-scripts/
          env:
            - name: EXTERNAL_IP
              valueFrom:
                configMapKeyRef:
                  name: {{ include "thor-daemon.fullname" . }}-external-ip
                  key: externalIP
            - name: VALIDATOR
              value: "{{ .Values.validator }}"
            - name: PEER
              value: {{ default .Values.peer .Values.global.peer }}
            - name: SEEDS
              value: {{ .Values.seeds }}
            - name: PEER_API
              value: {{ .Values.peerApi }}
            - name: DEBUG
              value: "{{ .Values.debug }}"
            - name: NET
              value: {{ include "thor-daemon.net" . }}
            - name: SIGNER_NAME
              value: {{ .Values.signer.name }}
            - name: SIGNER_PASSWD
              {{- if default .Values.signer.passwordSecret .Values.global.passwordSecret }}
              valueFrom:
                secretKeyRef:
                  name: {{ default .Values.signer.passwordSecret .Values.global.passwordSecret }}
                  key: password
              {{- else}}
              value: {{ .Values.signer.password }}
              {{- end }}
          ports:
            - name: p2p
              containerPort: {{ include "thor-daemon.p2p" . }}
              protocol: TCP
            - name: rpc
              containerPort: {{ include "thor-daemon.rpc" . }}
              protocol: TCP
            - name: prometheus
              containerPort: 26660
              protocol: TCP
          livenessProbe:
            timeoutSeconds: 10
            httpGet:
              path: /status
              port: rpc
          readinessProbe:
            timeoutSeconds: 10
            httpGet:
              path: /status
              port: rpc
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      volumes:
      - name: data
      {{- if and .Values.persistence.enabled (not .Values.persistence.hostPath) }}
        persistentVolumeClaim:
          claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "thor-daemon.fullname" . }}{{- end }}
      {{- else if and .Values.persistence.enabled .Values.persistence.hostPath }}
        hostPath:
          path: {{ .Values.persistence.hostPath }}
          type: DirectoryOrCreate
      {{- else }}
        emptyDir: {}
      {{- end }}
      - name: scripts
        configMap:
          name: {{ include "thor-daemon.fullname" . }}-scripts
          defaultMode: 0777
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
